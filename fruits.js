db.fruits.insertMany([
  {
    name : "Apple",
    color : "Red",
    stock : 20,
    price: 40,
    supplier_id : 1,
    onSale : true,
    origin: [ "Philippines", "US" ]
  },

  {
    name : "Banana",
    color : "Yellow",
    stock : 15,
    price: 20,
    supplier_id : 2,
    onSale : true,
    origin: [ "Philippines", "Ecuador" ]
  },

  {
    name : "Kiwi",
    color : "Green",
    stock : 25,
    price: 50,
    supplier_id : 1,
    onSale : true,
    origin: [ "US", "China" ]
  },

  {
    name : "Mango",
    color : "Yellow",
    stock : 10,
    price: 120,
    supplier_id : 2,
    onSale : false,
    origin: [ "Philippines", "India" ]
  }
]);



//[section] MongoDB Aggregation

/*
  MongoDB Aggregation
    -used to generate manipulated date and erform operations to creat filteredresults that helps in analyzing data
    - compare to doing CRUD operations, aggregation gives us access to manipulate, filter and compute results providing us with information to make us necessary development decision without having to create a frontend application.
*/

//Using the Aggregate Method

/*
  $match is used to pass the documents that meet the specified condition(s) to the next pipeline stage/aggregation process

      Syntax:
        {$match: {field:boolean value}}

*/

db.fruits.aggregate([
  {$match : {onSale:true}}
  ]);

//"$group" is used to group elements together and field-value pairs, using the data from the group elements
/*
    Syntax:
      {$group: {id: "value", fieldResult: "valueResult"}}
*/

db.fruits.aggregate([
  {$group: {_id:"$supplier_id", total: {$sum: "$stock"}}}
  ]);


//Aggregating Documents using 2 pipeline stages
/*
  SYNTAX:
    db.collectionName.aggregate([
      {$match: {field:boolean value}},
      {$group: {id: "value", fieldResult: "valueResult"}};
    ])
*/

  db.fruits.aggregate([
  {$match : {onSale:true}},
  {$group: {_id:"$supplier_id", total: {$sum: "$stock"}}}
  ]);

//Field Projection with aggregation

  /*
    "$project" can be used when aggregating data to include/exclude fields from the returned results
    
    -The field that is set to zero(0) will be excluded
    -The field that is set to one(1) will be included
    SYNTAX:
    {$project: {field: 1/0}}
  */

  db.fruits.aggregate([
    {$project: {_id:0}}
    ]);

  db.fruits.aggregate([
    {$match : {onSale:true}},
    {$group: {_id:"$supplier_id", total: {$sum: "$stock"}}},
    {$project: {_id:0}}
  ]);


//Sorting aggregated reults
/*
  $sort can be  used to change the order of the aggregated results
  -providing a value of -1 will sort the aggregated result in a REVERSE ORDER

        SYNTAX:
          {$sort{field: 1/-1}}
*/

  db.fruits.aggregate([
    {$match : {onSale:true}},
    {$group: {_id:"$supplier_id", total: {$sum: "$stock"}}},
    {$sort: {total:-1}}
  ]);

  //the field that is set to a value of negative (-1) was sorted in descending order
  //the field that is set to a value of positive (1) was sorted in ascending order


//Aggregating results based on the array fields
  /*
    "$unwind" deconstructs an array field from a collection with an array value to give us a results for each array elements

    SYNTAX:
      {$unwind: field}
  */

  db.fruits.aggregate([
    {$unwind: "$origin"}
    ]);
